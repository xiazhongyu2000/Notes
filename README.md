# Notes

> | 计算机专业基础综合 |       [考研](Postgraduate/Postgraduate.md)       |                    编程笔记                     |   日记   |
> | :----------------------------------------------------------: | :--------------: | :---------------------------------------------: | :------: |
> |                           数据结构                           |     高等数学     |                      [Algorithm](Algorithm/Algorithm.md)                      | 我的日记 |
> |                        计算机组成原理                        |     线性代数     |        C/C++        | 名言警句 |
> |                        计算机操作系统                        | 概率论与数理统计 |         [Java](Programming-Notes/Java.md) |          |
> |                          计算机网络                          |       英语       | Python |          |
> | [编译原理](Computer-Science-Professional-Foundation/Compilation-Principle.md) |   思想政治理论   | [Spring-Boot](Programming-Notes/Spring-Boot.md) |          |
> |                            数据库                            |                  | 微信小程序 |          |
>

![](https://xiazhongyu-1311258395.cos.ap-nanjing.myqcloud.com/GitHub/everydayonecat.PNG)

